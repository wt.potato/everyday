package com.xmg.gupao;

public class FinallyTest {

    public static void main(String[] args) {
        System.out.println("getValue:" + getValue());
    }

    public static int getValue() {
        try{
            return 0;
        } finally {
            return 1;
        }
    }
}
