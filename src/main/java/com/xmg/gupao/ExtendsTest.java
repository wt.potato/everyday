package com.xmg.gupao;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 类继承问题
 *
 * @author wtd
 */
 public class ExtendsTest {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        Class clazz = Class.forName("com.xmg.gupao.Child");

        Class superClazz = clazz.getSuperclass();

        Method[] methods = superClazz.getDeclaredMethods();
        AccessibleObject.setAccessible(methods, true);
        for (Method method:methods
             ) {
            System.out.println();
            System.out.println("子类调用方法：" + method.getName());
            method.invoke(new Child());
        }
    }
}

class Parent {
    public Parent() {
        System.out.println("父类构造方法");
    }

    public void method1(){
        System.out.println("父类public方法");
    }

    private void method2(){
        System.out.println("父类private方法");
    }

    private static void method3() {
        System.out.println("父类private static方法");
    }

    private final void method4() {
        System.out.println("父类private final方法");
    }

}

class Child extends Parent{

    public Child() {
        System.out.println("子类构造方法");
    }

    public void childMethod1() {
        System.out.println("子类public方法");
    }
}
