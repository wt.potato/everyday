package com.xmg.everyday.question;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/10 09:36
 */
public class Name {

    private String first, last;

    public Name(String first, String last) {
        if (first == null || last == null) {
            throw new NullPointerException();
        }
        this.first = first;
        this.last = last;
    }

    // 重写equals方法，但参数错误（参数Object）。Map，set自定义对象时必须重写hashCode与equals
    public boolean equals(Name o) {
        return first.equals(o.first) && last.equals(o.last);
    }

    public int hashCode(){
        return 31 * first.hashCode() + last.hashCode();
    }

    public static void main(String[] args) {
        Set s = new HashSet();
        s.add(new Name("Mickey", "Mouse"));
        System.out.println(s.size());
        // true，详细详解对比过程
        System.out.println(s.contains(new Name("Mickey", "Mouse")));
    }
}
