package com.xmg.everyday.question;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/18 09:51
 */
public class AnimalFarm {

    public static void main(String[] args) {
        // 1.运算符优先级问题 先进行连接字符串，之后进行
        // 2. == equals的区别 == 比较内存地址，equals比较内容
        // 3. 初始化变量pig时，在字符串常量池（内存中）开辟一块空间，
        // 初始化变量dog时先判断是否有相同的变量，一样指向同一块内存地址，不一样重新开辟一块空间。所以pig与dog的内存地址不一样
        // 4. intern()方法的功能 在常量池中查询是否有相同内容的变量，一样指向同一块内存地址

        final String pig = "length: 10";
        final String dog = "length: " + pig.length();
        System.out.println("Animal equals: " + pig + " == " + dog);
        System.out.println("Animal equals: " + pig == dog);
        System.out.println("Animal equals: " + pig.equals(dog));
        System.out.println("Animal equals: " + (pig == dog.intern()));

    }

}
