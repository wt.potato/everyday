package com.xmg.everyday.question;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/18 15:27
 */
public class Lazy {

    private static boolean initialized = false;

    static {
        Thread t = new Thread(() -> initialized = true);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(initialized);
    }
}
