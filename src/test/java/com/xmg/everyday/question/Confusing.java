package com.xmg.everyday.question;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/15 09:14
 */
public class Confusing {

    public Confusing(Object o) {
        System.out.println("Object");
    }
    public Confusing(double[] dArray) {
        System.out.println("double array");
    }

    public static void main(String[] args) {
        //most specific原则（最优匹配原则） 所有的double[]参数类型都是object，但不是所有的object都是double[]
        new Confusing(null);
    }
}
