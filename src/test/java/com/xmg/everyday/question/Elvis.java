package com.xmg.everyday.question;

import java.util.Calendar;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/17 09:09
 */
public class Elvis {
    public static final Elvis INSTANCE = new Elvis();
    private final int beltSize;
    private static final int CURRENT_YEAR =
            Calendar.getInstance().get(Calendar.YEAR);

    public Elvis() {
        System.out.println("CURRENT_YEAR:" + CURRENT_YEAR);
        beltSize = CURRENT_YEAR - 1930;
    }

    public int beltSize(){
        return beltSize;
    }

    public static void main(String[] args) {
        /*
        类加载，初始化（构造方法，静态变量，父子类）过程分析
            1.new Object 执行构造方法
            2.静态变量，代码块优先加载
            3.父子类：静态代码块，构造方法
                1.执行静态代码块（父类优先）
                2.执行构造方法（父类优先）
         */
        System.out.println("CURRENT_YEAR:" + CURRENT_YEAR);
        System.out.println("Elvis wears size " + INSTANCE.beltSize() + " belt.");
//        System.out.println("Elvis wears size " + new Elvis().beltSize() + " belt.");
    }
}
