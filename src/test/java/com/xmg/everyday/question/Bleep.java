package com.xmg.everyday.question;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/11 14:32
 */
public class Bleep {
    volatile String name = "Bleep";

    void setName(String name) {
        this.name = name;
    }

    void backGroundSetName() throws InterruptedException {
        Thread t = new Thread(){
            @Override
            public void run() {
                // 此方法与外部方法不是同一方法
                // public final synchronized void setName(String name)
                setName("Blat");
            }
        };
        t.start();
        t.join();
        System.out.println(name);
    }

    public static void main(String[] args) throws InterruptedException {
        // Bleep
        new Bleep().backGroundSetName();
    }
}
