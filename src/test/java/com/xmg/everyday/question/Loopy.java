package com.xmg.everyday.question;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/15 09:29
 */
public class Loopy {
    public static void main(String[] args) {
        final int start = Integer.MAX_VALUE - 100;
        final int end = Integer.MAX_VALUE;
        int count = 0;
        System.out.println(start);
        System.out.println(end);
        // i++ 达到Integer.MAX_VALUE的最大值的时候一直满足i == end
        // 以下循环会进入死循环
        for (int i = start; i <= end; i++) {
            count++;
        }
        System.out.println(count);
    }
}
