package com.xmg.everyday.ClassLoader;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/17 10:58
 */
public class Students extends Teacher {

    public Students() {
        System.out.println("Students Constructor");
    }

    static {
        System.out.println("Students static");
    }

    public static void methodA(){
        System.out.println("Students methodA");
    }

    public  void methodB(){
        System.out.println("Students methodB");
    }

    public static void main(String[] args) {
        Students students = new Students();
        students.methodB();
        Students.methodA();
    }

}
