package com.xmg.everyday.ClassLoader;

/**
 * TODO
 *
 * @author wt.d
 * @date 2019/7/17 10:58
 */
public class Teacher {

    public Teacher() {
        System.out.println("Teacher Constructor");
    }

    static {
        System.out.println("Teacher static");
    }

    public static void methodA(){
        System.out.println("Teacher methodA");
    }

    public  void methodB(){
        System.out.println("Teacher methodB");
    }

}
